/**
 * Created by wojtek on 26.03.14.
 */

$(document).ready(function () {


    var getTextFromRoot = function (root) {

        var $children = $(root).children();
        var teksty = [];

        _.each($(root), function(node){
            teksty.push(node.innerText);
        });

        return teksty.join('\n');
    };

    alert(getTextFromRoot($('.div2')));
});
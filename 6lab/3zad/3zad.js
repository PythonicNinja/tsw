/**
 * Created by wojtek on 26.03.14.
 */
$(document).ready(function () {


    var addBorder = function(div, howMuch){
        if(parseInt($(div).css('borderWidth'), 10)<howMuch ){
            div.style.border = howMuch+"px solid " +'#'+Math.floor(Math.random()*16777215).toString(16);
        }
        var parents = $(div).parents();
        if(parents){
            _.each(parents, function(parent){
                if($(parent).get(0).tagName == 'DIV'){
                    addBorder(parent, howMuch+2);
                }
            });
        }
    };
    var removeBorder = function(div){
        div.style.border = "";
    };

    $('body').on('mouseover','div',function(e){
        e.stopPropagation();
        addBorder(this, 1);
    });

    $('body').on('mouseout','div',function(e){
        removeBorder(this);
    });

});
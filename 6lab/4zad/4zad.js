/**
 * Created by wojtek on 27.03.14.
 */

$(document).ready(function () {

    var censor = function (reg) {
        var $children = $(document).children();
        while ($children.length > 0) {
            _.each($children, function(child){
                if(child.hasOwnProperty('firstChild') && child.firstChild!==null && child.firstChild.hasOwnProperty('data')){
                    while(match = reg.exec(child.firstChild.data)){
                        child.firstChild.data = child.innerText.replace(match[0], " CENZURA!!! ");
                        child.style.backgroundColor = "red";
                    }
                }
            });
            $children = $children.children();
        }
    }

    censor(/ cenzuruj/g);
});
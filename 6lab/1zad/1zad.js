/**
 * Created by wojtek on 26.03.14.
 */

$(document).ready(function () {


    var calcDepth = function (root) {
        var $children = $(root).children();
        var depth = 0;

        while ($children.length > 0) {
            $lastParent = $children;
            $children = $children.children();
            depth += 1;
        }

        any = _.some($lastParent, function(child){
            return child.innerText !== "";
        });

        if(any){
            depth+=1;
        }

        return depth;
    };

    console.log(calcDepth($('body')));

});
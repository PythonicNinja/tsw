/**
 * Created by wojtek on 02.04.14.
 */
domReady(function(){

    var accordianHover = function(event){
        event.target.nextElementSibling.style.display = 'block';
    };

    var accordianOut = function(event){
        if(event.target.getAttribute("data-show") === "true")
            event.target.nextElementSibling.style.display = 'block';
        else
            event.target.nextElementSibling.style.display = 'none';
    };

    var accordianClick = function(event){
        if(event.target.nextElementSibling.style.display === 'block'){
            event.target.setAttribute("data-show", "false");
            event.target.nextElementSibling.style.display = 'none';
        }
        else{
            event.target.setAttribute("data-show", "true");
            event.target.nextElementSibling.style.display = 'block';
        }



    };

    var headers = document.querySelectorAll('.hd');

    for(var i=0; i<headers.length; i++){
        headers[i].nextElementSibling.style.display = "none";
        headers[i].onclick = accordianClick;
        headers[i].onmouseover = accordianHover;
        headers[i].onmouseout = accordianOut;
    }

});
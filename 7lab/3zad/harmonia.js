/**
 * Created by wojtek on 02.04.14.
 */
domReady(function(){


    var accordianHover = function(event){
        event.target.nextElementSibling.style.display = 'block';
    };

    var accordianOut = function(event){
        if(event.target.getAttribute("data-show") === "yes")
            event.target.nextElementSibling.style.display = 'block';
        else
            event.target.nextElementSibling.style.display = 'none';

    };

    var accordianClick = function(event){

        if(event.ctrlKey){
            var element = event.target;
            var collapse;
            if(element.getAttribute("data-historical") == "no"){
                element.setAttribute("data-historical", "yes");
                collapse = true;
            }else{
                element.setAttribute("data-historical", "no");
                collapse = false;
            }

            while(element.nextElementSibling){
                element = element.nextElementSibling;
                if(element.className === 'hd'){
                    if(collapse){
                        if(element.style.display === 'block' || element.getAttribute("data-show") === 'yes'){
                            element.setAttribute("data-history", "yes");
                            element.setAttribute("data-show", "no");
                            element.nextElementSibling.style.display = 'none';
                        }else{
                            element.setAttribute("data-history", "no");
                            element.setAttribute("data-show", "no");
                            element.nextElementSibling.style.display = 'none';
                        }
                    }else{
                        if(element.getAttribute("data-history") === 'yes'){
                            element.nextElementSibling.style.display = 'block';
                            element.setAttribute("data-show", "yes");
                        }else{
                            element.nextElementSibling.style.display = 'none';
                            element.setAttribute("data-show", "no");
                        }
                    }
                }
            }
        }

        if(event.target.nextElementSibling.style.display === 'block'){
            event.target.setAttribute("data-show", "no");
            event.target.nextElementSibling.style.display = 'none';
        }
        else{
            event.target.setAttribute("data-show", "yes");
            event.target.nextElementSibling.style.display = 'block';
        }


    };

    var headers = document.querySelectorAll('.hd');

    for(var i=0; i<headers.length; i++){
        headers[i].nextElementSibling.style.display = "none";
        headers[i].setAttribute("data-show", "no");
        headers[i].setAttribute("data-history", "no");
        headers[i].setAttribute("data-historical", "no");
        headers[i].onclick = accordianClick;
        headers[i].onmouseover = accordianHover;
        headers[i].onmouseout = accordianOut;
    }

});
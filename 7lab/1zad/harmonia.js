/**
 * Created by wojtek on 02.04.14.
 */
domReady(function(){

    var accordian = function(event){
        if(event.target.nextElementSibling.style.display === 'block')
            event.target.nextElementSibling.style.display = 'none';
        else
            event.target.nextElementSibling.style.display = 'block';
    };

    var headers = document.querySelectorAll('.hd');

    for(var i=0; i<headers.length; i++){
        headers[i].nextElementSibling.style.display = "none";
        headers[i].onclick = accordian;
    }

});
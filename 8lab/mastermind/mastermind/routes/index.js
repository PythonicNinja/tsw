var __ = require("underscore");

exports.index = function (req, res) {

    res.render('index', {
        title: 'Mastermind'
    });
};

exports.play = function (req, res) {
    var newGame = function () {
        var i, data = [],
            puzzle = req.session.puzzle;
        for (i = 0; i < puzzle.size; i += 1) {
            data.push(Math.floor(Math.random() * puzzle.dim));
        }
        req.session.puzzle.data = data.join("");
        return {
            "status": "ok",
            "number": req.session.puzzle.data
        };
    };
    // poniższa linijka jest zbędna (przy założeniu, że
    // play zawsze używany będzie po index) – w końcowym
    // rozwiązaniu można ją usunąć.
    req.session.puzzle = req.session.puzzle || req.app.get('puzzle');
    /*
     * req.params[2] === wartość size
     * req.params[4] === wartość dim
     * req.params[6] === wartość max
     */

    if (req.params[2]) {
        req.session.puzzle.size = req.params[2];
    }
    if (req.params[4]){
        req.session.puzzle.dim = req.params[4];
    }
    if (req.params[6]){
        req.session.puzzle.max = req.params[6];
    }

    res.json(newGame());
};

exports.mark = function (req, res) {

    var toSet = function(string){
        var object = {};
        var list = [];
        for(var i=0; i<string.length;i++){
            object[string[i]]=1;
        }

        for (var key in object) {
            list.push(key);
        }

        return list                                                                                                                                                 ;
    }

    var scoreMove = function(move, valid){

        var zipped = __.zip(move,valid);
        var black = 0;
        __.each(zipped, function(element){
            if(element[0]===element[1]){black++;}
        });
        var common_elements = __.intersection(move,valid);
        var white = common_elements.length - black;

        return {
            'white': white,
            'black': black
        }
    };

    var displayMessage = function(move, valid){
        var retMsg;
        req.session.puzzle = req.session.puzzle || req.app.get('puzzle');

        if(move === valid)
            retMsg = "You win!";
        else if(req.session.puzzle.max === null || req.session.puzzle.count<=req.session.puzzle.max)
            retMsg = "Try again.";
        else
            retMsg = "You lose!";

        incrementCount();

        return retMsg
    }

    var incrementCount = function(){
        req.session.puzzle = req.session.puzzle || req.app.get('puzzle');
        if(req.session.puzzle.count){
            req.session.puzzle.count=req.session.puzzle.count+1;
        }
    }

    var markAnswer = function () {
        var move = req.params[0].split('/');
        move = move.slice(0, move.length - 1)[0];

        req.session.puzzle = req.session.puzzle || req.app.get('puzzle');
        var valid = req.session.puzzle.data;

        console.log(move,valid);

        return {
            "retVal": scoreMove(move, valid),
            "retMsg": displayMessage(move, valid)
        };
    };
    res.json(markAnswer());
};

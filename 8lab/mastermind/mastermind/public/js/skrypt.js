$(document).ready(function(){

    $('body').on('submit','#config', function(e){
        e.preventDefault();

        var size = $(this).find('#size').val();
        var dim = $(this).find('#dim').val();
        var max = $(this).find('#max').val();

        $(this).slideToggle("slow");
        $('#showConfig').slideToggle("slow");
        $('#config-result').show("slow");
        $('#config-size').text(size);
        $('#config-dim').text(dim);
        $('#config-max').text(max);


        $.ajax({
            url: "play/size/"+size+"/dim/"+dim+"/max/"+max+"/",
            type: "GET",
            context: this,
            error: function () {},
            dataType: 'json',
            success : function (response) {
                if(response.status === "ok"){
                    $('#empty-guess').find('input[name="guess"]').attr('maxlength',size);
                    $('#guesses').append($('#empty-guess').clone().show());
                }
                else{
                    alert("failure");
                }

            }
        });

    });

    $('body').on('click', '#showConfig', function(e){
       $(this).slideToggle("slow");
       $('#config').slideToggle("slow");
    });


    $('body').on('click', '.submit-guess', function(e){

        var guess = $(this).closest('tr').find('input[name="guess"]').val();
        $.ajax({
            url: "mark/"+guess+"/",
            type: "GET",
            context: this,
            error: function () {},
            dataType: 'json',
            success : function (response) {
                $(this).prop('disabled', true);
                $(this).closest('tr').find('input[name="guess"]').prop('disabled', true);
                console.log(response);
                if(response.retMsg === "Try again."){
                    $('#guesses').append($('#empty-guess').clone().show());
                    for(var i=0;i<response.retVal.black; i++)
                        $(this).closest('tr').find('.score').append($('#star-black').clone().show());
                    for(var i=0;i<response.retVal.white; i++)
                        $(this).closest('tr').find('.score').append($('#star').clone().show());
                }
                else if(response.retMsg === "You win!"){
                    alert(response.retMsg);
                }
                else if(response.retMsg === "You lose!"){
                    $(this).prop('disabled', true);
                    $(this).closest('tr').find('input[name="guess"]').prop('disabled', true);
                    alert(response.retMsg);
                }
            }
        });
    });


});
var http = require('http');
var express = require("express");
var __ = require("underscore");

var connect = require('connect');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var sessionStore = new connect.session.MemoryStore();

var app = express();

var httpServer = http.createServer(app);
var socketio = require("socket.io");
var passportSocketIo = require('passport.socketio');
var io = socketio.listen(httpServer);
var sessionSecret = 'wielkiSekret44';
var sessionKey = 'connect.sid';


// Konfiguracja passport.js
passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        if ((username === 'admin') && (password === 'tajne')) {
            console.log("Udane logowanie...");
            return done(null, {
                username: username,
                password: password
            });
        } else {
            return done(null, false);
        }
    }
));
app.use(express.cookieParser());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.session({
    store: sessionStore,
    key: sessionKey,
    secret: sessionSecret
}));

app.use(passport.initialize());
app.use(passport.session());


function defaultDict() {
    this.get = function (key) {
        if (this.hasOwnProperty(key)) {
            return this[key];
        } else {
            this[key]=[];
            return this[key];
        }
    }
}


var history = new defaultDict();

var rooms = {
    'główny': [],
    'śmiechowy': [],
    'testowy': []
};

var users = {};
var Socket2Nick = {};

app.use(express.static("public"));
app.use(express.static("bower_components"));


app.get('/login', function (req, res) {
    var body = '<html><body>'
    body += '<form action="/login" method="post">';
    body += '<div><label>Użytkownik:</label>';
    body += '<input type="text" name="username"/><br/></div>';
    body += '<div><label>Hasło:</label>';
    body += '<input type="password" name="password"/></div>';
    body += '<div><input type="submit" value="Zaloguj"/></div></form>';
    body += '</body></html>'
    res.send(body);
});

app.post('/login',
    passport.authenticate('local', {
        failureRedirect: '/login'
    }),
    function (req, res) {
        res.redirect('/');
    }
);

app.get('/logout', function (req, res) {
    console.log('Wylogowanie...')
    req.logout();
    res.redirect('/');
});

var onAuthorizeSuccess = function (data, accept) {
    console.log('Udane połączenie z socket.io');
    accept(null, true);
};

var onAuthorizeFail = function (data, message, error, accept) {
    if (error) {
        throw new Error(message);
    }
    console.log('Nieudane połączenie z socket.io:', message);
    accept(null, false);
};

io.set('authorization', passportSocketIo.authorize({
    passport: passport,
    cookieParser: express.cookieParser,
    key: sessionKey, // nazwa ciasteczka, w którym express/connect przechowuje identyfikator sesji
    secret: sessionSecret,
    store: sessionStore,
    success: onAuthorizeSuccess,
    fail: onAuthorizeFail
}));

io.set('log level', 2); // 3 == DEBUG, 2 == INFO, 1 == WARN, 0 == ERROR


io.sockets.on('connection', function (socket) {


	socket.on('send msg', function (username, data) {

        // Assign user nick to socket
        Socket2Nick[socket.id]=username;
        users[socket.id]['username'] = username;
        sendRoomsUsers();

        // add data to room
		history.get(room).unshift(data);
		//io.sockets.emit('rec msg', data);
        sendHistoryForRoom(room);
	});


    socket.on('change room', function(new_room){
        room = findRoomForSocket(socket.id);
        console.log('----->found room', room);
        if(room){
            deleteUserFromRoom(socket.id, room);
        }
        rooms[new_room].push(socket.id);
        sendRoomsUsers();
    });

    socket.on('get history for room', function(room){
        console.log(history);
        sendHistoryForRoom(room);
    });


    socket.on('disconnect', function(reason) {
        room = findRoomForSocket(socket.id);
        console.log('----->found room', room);
        if(room){
            deleteUserFromRoom(socket.id, room);
        }
        sendRoomsUsers();
    });


    var sendRoomsUsers = function(){
        for(user in users){
            users[user].emit('rooms', rooms);
            users[user].emit('users', Socket2Nick);
        }
    };

    var sendHistoryForRoom = function(room){
        socket.emit('history', history.get(room));
    };


    var findRoomForSocket = function(socket_id){
        for(room in rooms){
            console.log('room',room);
            console.log('users in room', rooms[room]);

            for(var i=0; i<rooms[room].length; i++){
                var user = rooms[room][i];
                console.log(user,socket_id,room);
                if(socket_id===user){
                    return room;
                }
            }
        }
        return false;
    };

    var deleteUserFromRoom = function(socket_id, room){
       var usersInRoom = rooms[room];

        var indexToRemove = usersInRoom.indexOf(socket_id);
        if (indexToRemove > -1) {
            rooms[room].splice(indexToRemove, 1);
        }
    };

    rooms['główny'].push(socket.id);
    users[socket.id] = socket;
    sendRoomsUsers();
    socket.emit('history', history.get(findRoomForSocket(socket.id)));
});

httpServer.listen(8000, function () {
    console.log('Serwer HTTP działa na pocie 8000');
});

var app = angular.module('czatApka', []);

app.factory('socket', function () {
    var socket = io.connect('http://' + location.host);

    return socket;
});

app.controller('chatCtrlr', ['$scope', 'socket',
    function ($scope, socket) {
        var tagsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };
        var replaceTag = function (tag) {
            return tagsToReplace[tag] || tag;
        };
        var safe_tags_replace = function (str) {
            return str.replace(/[&<>]/g, replaceTag);
        };
        $scope.msgs = [];
        $scope.rooms = {};
        $scope.users = {};
        $scope.user;
        $scope.connected = false;
        $scope.sendMsg = function () {
            if ($scope.msg && $scope.msg.text) {
                socket.emit('send msg', $scope.user, safe_tags_replace($scope.msg.text.substring(0, 20)));
                $scope.msg.text = '';
            }
        };

        $scope.changeRoom = function (room) {
            if (room) {
                socket.emit('change room', room);
                socket.emit('get history for room', room);
            }
        };

        socket.on('connect', function () {
            $scope.connected = true;
            $scope.$digest();
        });

        socket.on('history', function (data) {
            $scope.msgs = data;
            $scope.$digest();
        });

        socket.on('rooms', function (data) {
            $scope.rooms = data;
            $scope.$digest();
            console.log('recived rooms', data, $scope.rooms);
        });

        socket.on('users', function(data){
            $scope.users = data;
            $scope.$digest();
            console.log('recived users', data, $scope.users);
        });

        socket.on('rec msg', function (data) {
            $scope.msgs.unshift(data);
            $scope.$digest();
        });
    }
]);

/**
 * Created by wojtek on 19.03.14.
 */

var fib = function fib(arg) {
    if (arg <= 0) {
        return 0;
    }
    if (arg === 1) {
        return 1;
    }
    return fib(arg - 1) + fib(arg - 2);
};

var memo = function (cache, fun) {
    return function (x) {
      if(cache.length >= x){
          return cache[x-1];
      }else{
          cache[x-1]=fun(fibonacci, x);
          return cache[x-1];
      }

    };
};

var fibonacci = memo([0, 1], function (recur, n) {
    return recur(n - 1) + recur(n - 2);
});


try {
    console.log(fib(50));
} catch (e) {
    console.log(e);
}



/**
 * Created by wojtek on 19.03.14.
 */

var __ = require("underscore");

var szablon =
  '<table border="{border}">' +
  '  <tr><td>{first}</td><td>{last}</td></tr>' +
  '</table>';


String.prototype.podstaw = function(dane){
    var current = this;

    for(attr in dane){
        current = current.replace("{"+attr+"}", dane[attr]);
    }

    return current.toString();
}

var dane = {
    first: "Jan",
    last:  "Kowalski",
    pesel: "97042176329"
};


try {
    console.log(szablon.podstaw(dane));
} catch (e) {
    console.log(e);
}

/**
 * Created by wojtek on 19.03.14.
 */
'use strict';

var defFun = function(fun, types){
    fun.typeConstr = types;
    return fun;
};


var appFun = function(f){
    if(f.hasOwnProperty('typeConstr')){
        for(var i=1; i<arguments.length; i++){
            if(typeof arguments[i] !== f.typeConstr[i-1]){
                throw({ typerr: "Got "+ typeof arguments[i]+" expected "+f.typeConstr[i] });
            }
        }
        var args = Array.prototype.slice.call(arguments, 1);
        return f.apply(this, args);
    }else{
        throw({ typerr: "Function doesn't have property of typeConstr"});
    }
};


var myfun = defFun(function (a, b) {
    return a + b;
}, ['number', 'number']);


try {
    alert(appFun(myfun, 12, 15));
} catch (e) {
    alert(e.typerr);
}
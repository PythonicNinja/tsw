var express = require("express");
var __ = require("underscore");
var app = express();

var httpServer = require("http").createServer(app);

var socketio = require("socket.io");
var io = socketio.listen(httpServer);

function defaultDict() {
    this.get = function (key) {
        if (this.hasOwnProperty(key)) {
            return this[key];
        } else {
            this[key]=[];
            return this[key];
        }
    }
}


var history = new defaultDict();

var rooms = {
    'główny': [],
    'śmiechowy': [],
    'testowy': []
};

var users = {};
var Socket2Nick = {};

app.use(express.static("public"));
app.use(express.static("bower_components"));

io.sockets.on('connection', function (socket) {


	socket.on('send msg', function (username, data) {

        // Assign user nick to socket
        Socket2Nick[socket.id]=username;
        users[socket.id]['username'] = username;
        sendRoomsUsers();

        // add data to room
		history.get(room).unshift(data);
		//io.sockets.emit('rec msg', data);
        sendHistoryForRoom(room);
	});


    socket.on('change room', function(new_room){
        room = findRoomForSocket(socket.id);
        console.log('----->found room', room);
        if(room){
            deleteUserFromRoom(socket.id, room);
        }
        rooms[new_room].push(socket.id);
        sendRoomsUsers();
    });

    socket.on('get history for room', function(room){
        console.log(history);
        sendHistoryForRoom(room);
    });


    socket.on('disconnect', function(reason) {
        room = findRoomForSocket(socket.id);
        console.log('----->found room', room);
        if(room){
            deleteUserFromRoom(socket.id, room);
        }
        sendRoomsUsers();
    });


    var sendRoomsUsers = function(){
        for(user in users){
            users[user].emit('rooms', rooms);
            users[user].emit('users', Socket2Nick);
        }
    };

    var sendHistoryForRoom = function(room){
        socket.emit('history', history.get(room));
    };


    var findRoomForSocket = function(socket_id){
        for(room in rooms){
            console.log('room',room);
            console.log('users in room', rooms[room]);

            for(var i=0; i<rooms[room].length; i++){
                var user = rooms[room][i];
                console.log(user,socket_id,room);
                if(socket_id===user){
                    return room;
                }
            }
        }
        return false;
    };

    var deleteUserFromRoom = function(socket_id, room){
       var usersInRoom = rooms[room];

        var indexToRemove = usersInRoom.indexOf(socket_id);
        if (indexToRemove > -1) {
            rooms[room].splice(indexToRemove, 1);
        }
    };

    rooms['główny'].push(socket.id);
    users[socket.id] = socket;
    sendRoomsUsers();
    socket.emit('history', history.get(findRoomForSocket(socket.id)));
});

httpServer.listen(3000, function () {
    console.log('Serwer HTTP działa na pocie 3000');
});
